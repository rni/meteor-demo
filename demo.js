//Declare document collection on server AND client
Frameworks = new Mongo.Collection("frameworks");

//Declare Remote Procedure Calls
Meteor.methods({
  //remove all frameworks, and insert fixtures
  'resetData': function(){
    //remove all documents
    Frameworks.remove({});

    //RPCs are also run on the client! (stub-methods)
    if(Meteor.isServer){
      insertFrameworks();
    }
  }
});

//server only code
if (Meteor.isServer) {
  var insertFrameworks = function(){
    if(Frameworks.find({}).count() < 1){
      _.each([
        {'name': 'Sails JS', server: true},
        {'name': 'Meteor', client: true, server: true},
        {'name': 'DerbyJS', client: true, server: true},
        {'name': 'Angular', client: true},
        {'name': 'Knockout', client: true},
        {'name': 'EmberJS', client: true},
        {'name': 'ReactJS', client: true},
        {'name': 'expressJS', server: true},
        {'name': 'locomotiveJS', server: true}
      ], function(framework){
        Frameworks.insert(_.extend({}, framework, {votes: 0}));
      });
    }
  };
  Meteor.startup(function () {
    //check if fixtures need to be inserted
    insertFrameworks();
  });

  //Publish all frameworks to all connected clients
  Meteor.publish('frameworks', function(){
    return Frameworks.find({});
  });
}

//client only code
if (Meteor.isClient) {
  Meteor.startup(function () {
    //on client startup, subscribe to published frameworks
    Meteor.subscribe('frameworks');
  });

  Template.frameworks.helpers({
    //return all client-side cached frameworks
    //filter them, if filters are present in session
    //sort them on #votes
    frameworks: function () {
      var q = {};
      if(Session.get('client')){
        _.extend(q, {client: true});
      }
      if(Session.get('server')){
        _.extend(q, {server: true});
      }
      return Frameworks.find(q, {sort: {votes: -1}});
    },
    isActive: function(type){
      return !!Session.get(type)?'active':'';
    }
  });

  Template.frameworks.events({
    'click a.reset': function (event, template) {
      // reset all data, when reset-button is clicked
      Meteor.call('resetData');
    },
    'click a.client': function (event, template) {
      //set/change a reactive session variable when clicked on client button
      Session.set('client', !Session.get('client'));
    },
    'click a.server': function (event, template) {
      //set/change a reactive session variable when clicked on server button
      Session.set('server', !Session.get('server'));
    },
    'click li':function(event, template){
      //update local cache: increment #votes of clicked framework
      Frameworks.update({_id: this._id}, {$inc: {votes:1}});
    }
  });
}
